# Allgemeines
## Anwendungsbereich
Die Inventur „Verjüngungszustands- und Wildeinflussmonitoring (VWM)“ ist eine Waldinventur nach § 30 LWaldG, die landesweit in allen Eigentumsformen durchgeführt wird. Sie ist ein Instrument der Politikberatung und hat das Ziel, Informationen zur Verjüngung (nachwachsende Waldgeneration) sowie dessen Zustand zu erheben und darzustellen.
Erfasst werden das Vorhandensein, Art und Zustand von Verjüngung sowie Angaben zum Waldbestand. Die Verjüngung wird nach Art mit den Parametern Höhe, Entwickung und Dichte klassifiziert. Jeder Baum wird auf Verbiss-, Schäle- oder Fegeschäden der letzten 12 Monate untersucht. Zudem werden biotische als auch abiotische Entwicklungseinschränkungen erhoben. 
## Aufgaben des Auftraggebers

- Planung und Koordinierung des Einsatzes der Unternehmer
- Vorbereitung der Unterlagen für die Unternehmer
- Bereitstellung der Aufnahmesoftware und Stichprobenpunkte
- Schulung der Inventurtrupps zur einheitlichen Durchführung der Inventur
- Erörterung aller Fragen, die für die Gewährleistung einer einheitlichen Durchführung der Bundeswaldinventur von Bedeutung sind
- Prüfung der Daten auf Plausibilität
- Sicherstellung der ordnungsgemäßen Erhebung der Daten, einschließlich Inventurkontrolle 
- Auswertung der Daten
## Aufgaben des Auftragnehmers
### Personal
Für die Inventurarbeiten sind ausschließlich fachlich geeignete, ausreichend qualifizierte (Berufsjäger, forstliche Ausbildung oder Studium), sachkundige und geschulte Arbeitskräfte, welche die Grundlagen der Forstwirtschaft und des Naturschutzes verinnerlicht und in der Lage sind diese präzise umzusetzen, einzusetzen.
### Fristen
Der Auftragnehmer verpflichtet sich zu einer kontinuierlichen Abarbeitung, sodass nach einer regelhaften Abarbeitung bis zum 

- 15.06. des Jahres 25 %, 
- bis zum 15.08. des Jahres 50 %,
- bis zum 15.09. des Jahres 75 % und 
- bis zum 15.10. des Jahres 100 % 

der zugewiesenen Stichprobenpunkte abgearbeitet sind.

### Rechtliche Hinweise
Der Auftragnehmer verpflichtet sich sowie seine Erfüllungsgehilfen vor Beginn der Arbeiten, die untere Forstbehörde (örtlich zuständige Forstbehörde) und gegebenenfalls das zuständige Veterinäramt über Ort und voraussichtlichen zeitlichen Verlauf der Inventuraufnahme zu informieren sowie die Fahrzeugtypen und die amtl. Kennzeichen der Fahrzeuge, mit denen die Wälder befahren werden, zu melden und sich über etwaige Wald-, Wege- und ASP-Sperrungen zu informieren. Die Oberförstereien sind im geopackage „Hintergrunddaten“ im Layer „obf_points_contact“. Der Auftragnehmer verpflichtet sich, ein Informationsschreiben (VWM, Namen, Mobiltelefonnummer, im Auftrag LFB) sichtbar im genutzten Fahrzeug abzulegen. Weiterhin ist der Auftragnehmer verpflichtet die Rettungswege freizuhalten, sich über die Standorte der Rettungspunkte zu informieren und sein Fahrzeug ohne Gefährdung des Waldes und anderer im Wald Tätigen abzustellen.

Stichprobenpunkte in Horstschutzzonen sind erst ab 1.7.2023 aufzunehmen. 

Das Waldgesetz ist einzuhalten.

# Datenmanagement
## Inventurdaten und Software
Die Stichprobenpunkte (SP), werden vom Auftraggeber vorgegeben und digital als Geoobjekt übergeben. Sämtliche für die VWM erhobenen Daten werden digital in mobilen Datenerfassungsgeräten in der aktuellen Version von QGIS aufgenommen. Für die Inventur werden Dateien mit der nötigen Datenstruktur und den Stichprobenpunkten an denen jeweils Aufnahmen erfolgen, zum Download bereitgestellt.

## Abgabe der Daten
Der Auftragnehmer führt in regelmäßigen Abständen, mindestens einmal wöchentlich, eine Synchronisation der Datenbank mit den Stichprobenpunkten und Erfassungen auf seinem Gerät mit der zentralen Datenbank durch. Der Auftragnehmer kontrolliert dabei den korrekten Upload der erfassten Stichprobenpunkte anhand des Synchronisationsprotokolls und meldet Widersprüche zu seinen Aufzeichnungen umgehend bei der Inventurleitung an. Die Übergabe soll wie folgt geschehen:
1. Öffnen des Ordners Datenrücklieferung

![Bild Ordnerauswahl](pics/DatenruecklaufOrdnerauswahl.png "Ordnerauswahl und  Öffnung, Dateiupload")

2. Klick auf kleines „+“ im linken oberen Bereich des Fensters
3. Auswahl der Option „Datei hochladen“
4. Auswahl der Datei im Explorer, Klick auf „Öffnen“

![Bild Ordnerauswahl] ("Auswahl der Datei")

5. Datei im Ordner umbenennen mit Datum und Truppkürzel

![Bild Ordnerauswahl] ( "Ordner umbenennen")

6. Benachrichtigungs-E-Mail an torsten.wiebke@lfb.brandenburg.de 

## Prüfung und Kontrolle 
Stichprobenpunkte mit Daten, die, bei der automatischen Prüfung oder nach Kontrollaufnahmen als Fehlerhaft erkannt werden, gehen zur Wiederholungsaufnahme an den Auftragnehmer zurück. Der Arbeitgeber ist verpflichtet, die Ausführungen der Arbeiten durch Abgleich mit der Datenbank des Landeskompetenzzentrum Forst Eberswalde zu kontrollieren. Vorgefundene Mängel sind von dem Auftragnehmer umgehend zu beheben. Werden Mängel nicht beseitigt, zählt die Leistung als nicht erbracht und ist nicht Abrechnungsfähig.

# Methode des Verjüngungszustands- und Wildeinflussmonitorings (VWM)
## Stichprobenverteilung und Inventurzeitraum

Bild 4

Die Stichprobenpunkte verteilen sich auf einem Netz aus Hexagonen welche sich wie Waben über das ganze Inventurgebiet legt (siehe https://h3geo.org/). Dies bietete einen festgelegten Verdichtungsalgorithmus in dem sich eine Wabe immer in dieselbe Anzahl an Waben teilt. Die SPs können demnach näher oder weiter auseinander liegen. Die meisten befinden sich jedoch auf Zoomstufe 8, mit ungefähr 872 m Abstand zueinander. Ein SP ergibt sich nur wenn sich der Zentroid des Hexagones  im Wald befindet. In ca. 4-5 Jahren ist das ganze Land einmal aufgenommen, wobei es Dauerbeobachtungsflächen mit jährlicher Aufnahme im Landeswald gibt. Der Zeitraum der Inventur befindet sich in der Vegetationsperiode (April bis September), um die Artenbestimmung am Blatt durchführen zu können. Dementsprechend kann der Zeitraum je nach Zustand der Vegetation angepasst werden.

## Der Aufnahmeplot
Der Aufnahmeplot besteht aus dem Stichprobenpunkt (mit GNSS einmessen), dem Aufnahmepunkt (Beginn des Transektes), 2 Baumplots und einen Transekt, welches beide Baumplots verbindet. Die Koordinaten führen zum Stichprobenpunkt und damit Mittelpunkt des ersten Baumplots. Mit 2 Meter Entfernung in Richtung Norden (0 Gon) beginnt das Transekt. Dieses ist 20 m lang und endet mit dem zweiten Baumplot. 

# Aufnahmeanweisung

Bild 5

Die VWM Inventur ist als ein Personen-Verfahren, bei welchem alle Aufnahmen durch eine einzelne Person erfolgen, möglich. Die Datenerfassung findet am Aufnahmeplot statt. Es handelt sich um eine objektive Ist-Zustand Erhebung, dementsprechend geht es nicht darum, zu interpretieren oder zu vermuten. Das Verfahren ist in 5 Abschnitte gegliedert. Die ersten 3 Abschnitte dienen dazu, das Transekt in den folgenden Jahren möglichst genau wieder finden zu können. Im ersten „SP-Aufsuchen“ werden die Truppdaten, die Zugänglichkeit und die Koordinaten des Stichprobenpunktes erfasst. In Abschnitt 2 und 3 die beiden Baumplots mit den Daten der jeweils max. 5 Bäumen. Im vierten Abschnitt geht es um das „Transekt“, in welchem sowohl der Verbiss, weitere Zustandsparameter als auch Vegetationsdaten erhoben werden. Letztlich folgt die Einbettung in die Umwelt durch die „Bestandesbeschreibung“.

## Stichprobenpunkt Aufsuchen
### Aufsuchen
Nachdem das QGIS Projekt erfolgreich gestartet und alle Makros aktiviert sind, wird die Soll-Koordinate des Stichprobenpunktes mit GNSS-Unterstützung aufgesucht und temporär markiert (Abb.7).
In der Aufnahmesoftware wird die Bearbeitung mittels des Werkzeuges „Bearbeitungsstatus umschalten“ (Abb. 8), zu finden in der Attributwerkzeugleiste, aktivieren. Der SP wird durch „Objekte über Rechteck oder Einzelklick wählen“ (Abb. 9) angewählt. Durch Klick auf die Kachel „Attribute von allen gewählten Objekten gleichzeitig ändern“ (Abb. 10) öffnet sich das Aufnahmeformular (Abb. 11), mit fünf verschiedenen Reitern. 

Bild 6, 7, 8, 9

Zunächst wird der erste Reiter „SP-Aufsuche“ bearbeitet. In diesem wird sowohl das Kürzel des Aufnahmetrupps, der Hersteller und Modell des GNSS Empfängers als auch Informationen zum SP erfasst. Das Aufnahmtruppkürzel dient vor allem innerbetrieblich der Qualtitätssicherung. Falls dieser nicht Aufgesucht werden kann, wird in „Zugänglichkeit“ die passende Begründung ausgewählt. Danach werden über die Schaltfläche „Messung starten“ die Koordinate ermittelt. Dieser Vorgang kann einige Zeit in Anspruch nehmen, da 100 Messungen durchgeführt werden, in dieser Zeit muss der GNSS-Empfänger am Stichprobenpunkt verbleiben und es sollen keine weiteren Eingaben in QGIS getätigt werden. Ist der vorgegebene Punkt nicht im Wald im Sinne des §2 des LWaldG Brandenburgs kann er verlegt werden (Siehe 4.2.1.). Falls dieser nicht sinnvoll (forstüblich und in ca. 50 m Abstand) in eine Waldfläche verlegbar ist, ist der Punkt als „Nichtwald“ mit dem vorhandenen Landschaftselement in der Aufnahmesoftware zu erfassen. Liegt der Aufnahmepunkt in einem oder unmittelbarer Nähe zu einem besonders geschützten Biotop ist dieses in der Aufnahmesoftware zu erfassen.

Bild 10 und 11

 Wenn alle eingegebenen Daten durch einen grünen Haken abgehakt sind, kann der erste Baumplot aufgenommen werden.

### Verlegung des Stichprobenpunktes
Der Punkt ist um maximal 50 m zu verlegen, wenn:
- er auf einer Straße (Land-, Bundes-, Kreisstraße) liegt;
- er näher als 15 m an einer Straße liegt;
- er auf einem Haupt- oder Zubringerweg liegt;
- er auf einer Trasse oder anderen dauerhaft unbestockten Fläche liegt;
- er in einem dauerhaft abgesperrten Areal (z.B. gezäuntes Gelände) liegt.

Vom Auszuführenden ist dann ein geeigneter Ersatzpunkt zu suchen, der innerhalb eines Umkreises von maximal 50 m angelegt wird. Der Verlegungsgrund ist zu dokumentieren, die sich ergebenden Ist-Koordinate mittels GNSS einzumessen und in der Aufnahmesoftware zu erfassen. 

## Baumplot 1
### Im Gelände
Vom Stichprobenpunkt, sind im Umkreis von maximal 12 m von fünf Bäumen ab Derbholzstärke (BHD > 7 cm) die Baumart, der Azimut in Gon, die Entfernung in cm, der BHD in mm sowie das Vorhandensein von Schäle oder Fege der letzten 12 Monate aufzunehmen. Gestartet wird an dem nächstgelegenen Baum und die Aufnahme erfolgt möglichst im Uhrzeigersinn. Zur Aufnahme des BHD ist ein Umfangsmaßband bei 1,30 m anzulegen, z.B. bei einem Zwiesel kann die Messhöhe verändert werden, dies muss dann angegeben werden. Tote, nahezu tote Bäume oder zur Entnahme markierte werden nicht aufgenommen.

Bild 12, 13 und 14

### In der Software
#### Baumplot 1
In der Aufnahmesoftware werden die Bäume im Reiter „Baumplot 1“ aufgenommen. Dazu wird mithilfe des rechten der drei Symbole „Kindobjekt hinzufügen“ ein Baum angelegt (Abb. 16).

Bild 15 und 16

 Es öffnet sich ein weiteres Fenster, in welchem die Baumart aus einem Drop-Down-Menü ausgewählt wird und die Messwerte jeweils eingetragen werden (Abb. 17). 
#### Landmarken
Sind geeignete Landmarken als Orientierungspunkte vorhanden, sollen diese mit Art, Abstand und Azimut aufgenommen werden. Sind im Umkreis von 12 m um den Stichprobenpunkt keine Bäume mit einem BHD > 7 cm vorhanden, sind geeignete Orientierungspunkte/ -hilfen zu erfassen. Geeignete Orientierungspunkte sind unbewegliche Objekte wie z.B.: entfernt stehende Bäume, Gebäude, Masten, usw. Die Landmarken werden analog zu den Bäumen angelegt und eingetragen. 

Bild 17

#### Transektanlage
Zum Schluss wird der Winkel aufgenommen, im welchem das Transekt angelegt wird. Im Normalfall beträgt dieser 0 Gon, kann aber durch Störungen (4.1.2. sowie Rückegassen) im Uhrzeigersinn verändert werden (Abb. 18). 
## Baumplot 2: Am Ende des Transektes
Am Ende des anzulegenden Transektes wird eine weitere zweite Fünf-Baum-Stichprobe aufgenommen. Die Beurteilung der fünf Bäume erfolgt analog des Baumplots 1.
## Verjüngungszustanderfassung im Transekt
### Im Gelände
Der Anfang des Transektes ist in zwei Meter Entfernung vom SP in der Regel in Richtung Norden (0 Gon) anzulegen. Eine Fluchtstange markiert diesen Punkt. Mithilfe einer Bussole, Laser- bzw. Ultraschallentfernungsmesser oder Maßband ist eine Strecke von 20 m Länge zu ermitteln. Das Ende wird mit einer weiteren Fluchtstange markiert und dort der Baumplot 2 erstellt. Die Strecke zwischen den beiden Fluchtstangen bildet mit einer Breite von 2 m das Verjüngungstransekt. In diesem werden bis zu 50 Baume untersucht. 
### In der Software
#### Transektanlage
Der Reiter „Transekt“ teilt sich in Transektanlage, Verjüngungstransekt, Weiserpflanzen sowie Transektinfo (Abb. 20). Falls das Transekt nicht aufgenommen werden kann, sind die Gründe in der Transektanlage aufzunehmen. Hierfür können zum einen Schutzmaßnahmen im Drop-Down Menu ausgewählt oder manuell sonstige temporäre Störungen eingetragen werden.

Bild 18, 19 und 20

#### Verjüngungstransekt
Die Daten des Verjüngungstransektes sind analog zum Baumplot zu erheben. Der einzelne Verjüngungsbaum ist über die Kachel „Kindobjekt hinzufügen“ aufzunehmen (4.2.2.1). In dem sich öffnenden Fenster (Abb. 21) sind alle Bäume mit Baumart, Baumhöhenstufe, bei Bäumen über 2 m Höhe auch der BHD in mm, und dessen Zustand zu erheben. 

Bild 21

##### Baumart
In folgender Tabelle sind alle aufzunehmenden Arten zu finden. Ganz links ist die Nummer nach der die Baumarten sortiert sind. 

Bild 22, 23 und 24

Falls mehrere Schösslinge aus einem Spross gewachsen sind, werden sie gruppiert und als eine Verjüngungspflanze aufgenommen. Für die Höhenangabe und die Zustandserfassung zählt der höchst gewachsene Schössling.
Artenkenntnis wird vorausgesetzt, bei Unsicherheiten bezüglich der Art, wie es z.B. bei der Eiche geschehen kann, ist eine Artzuweisung bei ziemlicher Sicherheit zu vollziehen, ansonsten ist der Baum der Kategorie „Spec.“ zuzuordnen.

##### Baumhöhen
Die Baumhöhen sind ohne die Pflanze aufzurichten in folgenden Stufen zu erheben:
10: ab 10 cm bis 25 cm
26: bis 50 cm
51: bis 100 cm 
101: bis 150 cm
151: bis 200 cm
201: Ab Höhe >200 cm ist der BHD in mm mit einem Umfangsmaßband zu erfassen, die Angabe einer Höhenstufe entfällt dann.
Die Bäume werden nicht gestreckt.

##### Zustandserfassung
Die Zustandserfassung bezieht sich auf den Triebverlust - durch Verbiss, Trockenheit, Frost, Insekten oder Schäl- und Fegeschäden - im oberen Drittel der Pflanze der in den letzten 12 Monaten verursacht wurde. Im Zweifelsfall orientieren Sie sich an den Internodien. Je nach Aufnahmezeitpunkt kann ein Ersatztrieb schon ausgetrieben sein. Beim Triebverlust durch Trockenheit ist darauf zu achten, dass die Triebe eventuell durch Beschattung abgestoßen wurden.
Ältere Schäden sind nicht zu erfassen. Auch dann nicht, wenn sie eindeutig eine schlechte Holzqualität erwarten lassen.

Bild 25, 26, 27, 28 und 29

##### Transektlänge
Es sind alle Verjüngungspflanzen (Waldbäume) bis zu einer BHD von 7 cm aufzunehmen. Bei Erreichen der Maximalanzahl von 50 Pflanzen kann die Aufnahme beendet und die aufgenommene Transektlänge - in dm - in die Aufnahmesoftware übernommen werden (Abb. 28).

Bild 30

#### Weiserpflanzen
Weiterhin sind bestimmte Indikatorpflanzen mit ihrem Deckungsanteil zu erfassen: die Weiserpflanzen (Abb. 29). Hierfür wird wieder auf „Kindobjekt hinzufügen“ geklickt. Es öffnet sich ein neues Fenster „Weiserpflanzen im Transekt“. Hier geht es darum den Anteil einer Art auf der Fläche des Transektes zu schätzen. Die Gesamtdeckung kann dabei über 100% betragen. Alle Arten werden in diesem einen Fenster erfasst.

Bild 31

#### Transektinfo
In der Transektinfo ist, falls vorhanden, Signifikanter Fraß von Nagetieren an Verjüngungspflanzen anzugeben. Signifikant ist es, wenn die gesamte Verjüngung des Transektes nachhaltig beeinflusst wird. 
Letzten Endes ist der gesamte Deckungsanteil durch krautige Pflanzen zu erfassen. Hierzu zählen weder Moose noch Bäume. 

## Bestandesbeschreibung
### Im Gelände
Für die „Bestandesbeschreibung“ begibt sich der Aufnehmende an den Anfangspunkt und schaut Richtung Transekt. Erfasst wird der Waldausschnitt um das Transekt, 25 x 25m im Halbkreis (Abb. 30). Dabei wird die Betriebsart, der Kronenschlussgrad, eventuelle Schutzmaßnahmen, der Anteil des Unterstandes und der Kraut- und Strauchschicht sowie der Heterogenitätsgrad eingetragen.

Bild 32

### In der Software
#### Betriebsart
Die Betriebsart wird per Auswahl aus der Liste aufgenommen und ist eine der folgenden Möglichkeiten:
•	Hochwald - HW: Der Wald ist größtenteils aus Kernwüchsen entstanden. - Standard in Brandenburg
•	Mittelwald - MW: Der Wald besteht aus Unterholz aus Stockausschlägen und Oberholz aus Kernwüchsen. 
•	Niederwald - NW: Wald der sich ausschließlich durch Wurzelbrut und Stockausschlag verjüngt und in einer Umtriebszeit von 5 - 30 Jahren bewirtschaftet wird.
•	Plenterwald - PL: Sonderform des Hochwaldes mit stufiger Struktur in den Bäumen aller Alters- und Höhenstufen auf kleinster Fläche nebeneinanderstehen und die Nutzung ausschließlich Einzelstammweise erfolgt.

#### Kronenschlussgrad
Die Angabe des Kronenschlussgrades dient dazu die Lichtdurchlässigkeit bis auf den Boden zu klassifizieren. Für die Ansprache sind also alle Schichten über der Unterstandsschicht einzubeziehen. Der Kronenschlussgrad ist hier eine gutachterliche Einschätzung zur Lichtdurchlässigkeit und entspricht im Regelfall in etwa dem Bestockungsgrad in Klammern.
- gedrängt: Die Kronen greifen tief ineinander (ca. B° 1,1)
- geschlossen: Die Kronen berühren sich mit den Zweigspitzen, der Kronenschluss ist erreicht (ca. B° 1,0)
- locker: Kronen haben Abstand, ohne dass eine weitere Krone dazwischen Platz findet (ca. B° 0,8)
- lückig: Kronen haben Abstand, ab und an findet eine Krone Platz
- licht: Der Kronenabstand entspricht einer Kronenbreite (ca. B° 0,6)
- räumig/ räumdig: Der Kronenabstand überschreitet eine Kronenbreite (ca. B° < 0,4)


#### Schutzmaßnahmen
Schutzmaßnahmen, die sich nicht auf Einzelpflanzen im Verjüngungstransekt beziehen, aber im Halbkreis der Bestandesbeschreibung zu finden sind, werden erfasst. Dabei geht es nur um wirksame Schutzmaßnahmen. Wenn zu erkennen ist, dass die Fläche gezäunt, der Zaun aber wilddurchlässig ist, ist dieser nicht zu erfassen. 
Achtung: Stichprobenpunkte, die sich in einer forstlich gezäunten Fläche (z.B. Kultur) befinden, sind grundsätzlich aufzunehmen. Das Betreten dieser Fläche erfolgt nur an sicheren Ein- und Überstiegen. Jegliche Eigengefährdung, eine Gefährdung des Schutzgrundes und eine Beschädigung der Schutzanlage ist zu vermeiden. Dabei entstehende Beschädigungen und nachlaufende Entschädigungsforderungen gehen zu Lasten des Verursachers, in diesem Fall Aufnahmetrupp und Auftragnehmer.

#### N-Schichtig
Der vor dem Aufnehmenden liegende Bestand soll nun in seinen verschiedenen Schichten aufgenommen werden. Eine weitere Schicht ist bei einer eindeutigen anderen neuen Stuktur auszuweisen. Folgende Möglichkeiten stehen zur Wahl:
- Einschichtig 				OS und/oder Hb
- Zweischichtig				US u OS und/oder Hb
- Zweischichtig mit Überhälter		US und UeH und OS und/oder Hb
- Zweischichtig mit Vorausverjüngung	Verjüngung unterm Schirm und OS und/oder Hb
- Zweischichtig mit Unterbau		US und OS und/oder Hb
- Mehrschichtig oder plenterartig		Plenterwald
- Aus Ober-, Mittel- und Unterholz		US und MH und OS und/oder Hb

Signifikante Schichten können in 1 % Schritten aufgenommen werden. 

P = keine Zuordnung möglich (Plenterwald)

Hb = Hauptbestand – wirtschaftliches Hauptgewicht

US = Unterstand – erkennbar unterhalb des Oberstandes, jedoch dessen halbe Höhe nicht erreichend und noch keinen Derbholzvorrat aufweisend

OS = Oberstand

MH = Mittelholz, Zwischenstand – erkennbar unterhalb des Oberstandes, jedoch mindestens dessen halbe Höhe erreichend und einen Derbholzvorrat aufweisend

UeH = Überhalt – Reste des Vorbestandes mit annähernd gleichmäßiger Verteilung auf der Fläche

AB = Altbäume – alte Einzelbäume, die nicht der Schicht des Überhaltes zugeordnet werden können – Restvorrat

#### Geschütztes Biotop
Soweit es einzuschätzen ist, wird falls zutreffend eine der Schutzkategorien ausgewählt.
#### Bedeckungsgrad Unterstand
Hier wird der Bedeckungsgrad des Unterstandes in Prozent eingegeben.
#### Bodenbedeckung mit Gräser, Kräutern und Sträuchern
Der Prozentsatz aller Gräser, Kräuter und Sträucher, jedoch kein Moos sind hier einzutragen. Die Summe darf 100 % nicht überschreiten.
#### Heterogenitätsgrad
Die Struktur des Bestandes wird als Heterogenitätsgrad auf einer Skala von 1 = sehr homogene Struktur bis 10 = sehr heterogene Struktur angegeben. Es ist eine gutachterliche Einschätzung zur momentanen vertikalen und horizontalen Struktur. Bestimmend sind der Artenreichtum, die Schichtung, das Alter sowie stehendes und liegendes Totholz.
Beispiele:
niedrige Heterogenität: Einschichtiger Kieferwald im mittlerem Baumholz mit Heidelbeere, ohne Totholz, flaches Gelände
hohe Heterogenität: wenn verschiedene Kräuter und Sträucher sowie viele Bäume und Baumarten in unterschiedlichen natürlichen Altersklassen sowie liegendes und stehendes Totholz in verschiedenen Stärken.

Bild 33 und 34

#### Bestockung
Ungeachtet möglicher Bestandesabgrenzungen ist für jede Schicht die vorkommende Baumart mit natürlicher Altersklasse, Verteilung als auch ihrer Entstehung und Anteil als Zeile in die Standpunktbeschreibung aufzunehmen (Abb. 32). Baumarten mit einem Anteil kleiner 1 % werden nicht aufgenommen. Der Anteil aller Baumarten einer Schicht darf 100 % nicht übersteigen. 
##### Schicht
Zur Erstellung einer neuen Schicht wird ein neues „Kindobjekt hinzugefügt“ (Abb. 33). Ein neues Fenster erscheint in welchem die Angaben der Schicht eingefügt werden (Abb. 34). Innerhalb einer Schicht werden die Baumarten ab einem Deckungsgrad von 5% aufgenommen. Die Schichtzuordnung ist folgendermaßen vorzunehmen:

P = keine Zuordnung möglich (Plenterwald)

Hb = Hauptbestand – wirtschaftliches Hauptgewicht

US = Unterstand – erkennbar unterhalb des Oberstandes, jedoch dessen halbe Höhe nicht erreichend und noch keinen Derbholzvorrat aufweisend, ab Kniehöhe aufzunehmen

OS = Oberstand

VuS = Verjüngung unter Schirm

l= liegender Baum / sehr schräg sethend

MH = Mittelholz, Zwischenstand – erkennbar unterhalb des Oberstandes, jedoch mindestens dessen halbe Höhe erreichend und einen Derbholzvorrat aufweisend

UeH = Überhalt – Reste des Vorbestandes mit annähernd gleichmäßiger Verteilung auf der Fläche

AB = Altbäume – alte Einzelbäume, die nicht der Schicht des Überhaltes zugeordnet werden können – Restvorrat

Bild 35, 36 und 37

##### Baumart
Analog der Baumarten im Transekt.
##### Natürliche Altersklasse
Die natürlichen Altersklassen sind wir folgt aufgeteilt:

Bild 38

##### Entstehungsart
Die Art und Weise wie ein Bestand entstanden ist, kann nicht immer zweifelsfrei erkannt werden. Nur wenn es eindeutig sichtbar und keine Vermutung ist soll eine der folgenden Kategorien ausgewählt werden:
Naturverjüngung, Saat, Pflanzung, Stockausschlag, Anflug/Aufschlag, Hähersaat, Sukzession

##### Verteilung
Je nach Vorkommen ist eine der folgenden Kategorien zu wählen:

Stammweise: Einzelstämme auf der ganzen Fläche

Truppweise: Fläche mit Durchmesser bis 15m (ca. ½ Baumhöhe Endbestand)

Gruppenweise: Fläche mit Durchesser 16 bis 30m (ca. Baumhöhe Endbestand)

Horstweise: Fläche mit Durchmesser 31 bis 60m (ca. 2x Baumhöhe Endbestand)

Flächenweise: Größer als Horstweise

Reihenweise: Reihenweise Mischung/Wechsel der Baumarten

Streifenweise: Beimischung von mehreren Reihen einer Baumart bis 30m Breite

Der Einfachheit halber wurde dieselbe Codierung auch für die Bodenpflanzen gewählt. Wenden Sie das Konzept bestmöglich an.

##### Baumartenanteil
Hier ist der Anteil der Art in Prozent in der jeweiligen Schicht anzugeben. Eine Schicht hat insgesamt 100%. 
#### Bodenvegetation

Bild 39, 40 und 41

Für einige Vegetationsgruppen der Kraut- und Strauchschicht ist der Bedeckungsanteil anzugeben. Sobald die Kachel „Kindlayer hinzufügen“ angeklickt wurde (Abb. 37), öffnet sich ein Aufnahmeformular in welchem eine Vegetationsgruppe in % erhoben wird (Abb. 38). Deren Summe darf 100 % überschreiten. Die Verteilung ist analog der Bestockung vorzunehmen (Abb. 39).
Wenn Sie die Gruppen auseinanderhalten können, wovon wir ausgehen, wählen Sie die Unterkategorie, beispielsweise Süß- und oder Sauergras. Die Kategorien welche mit „%“ gekennzeichnet sind, können ausgewählt werden, die anderen bitte nicht.


	Gras:
-	Süßgras %
-	Sauergras: 
        Binsen %
        
	Kraut
-	Brennnessel %
-	Doldengewächse %
-	Labkraut %
-	Goldnessel %
-	Großsträucher %

	Kleinsträucher %

	Heidesträucher %

	Moose %

	Farne %

#### Störungen im Bestand
Wenn auf der Fläche des Halbkreises aktuelle, verjüngungsbeeinflussende Störungen zu erkennen sind, sollen diese erfasst werden. Als Störungen gelten Durchforstungen, Sanitärhiebe, Waldbrände, Stürme und Bodenbearbeitung wenn diese noch einen Einfluss auf die Verjüngung haben.
#### Workflow

Bild 42

Am Ende der Aufnahmen wird der Workflow automatisch um eine Nummer nach oben gesetzet (Abb. 40). Dadurch verändert sich die Farbe des Stichprobenpunktes in der Kartenansicht von Rot zu Grün.
## Speichern der Aufnahmen

Bild 43

Zum Speichern der Daten wird zum Schluss auf „OK“ geklickt. Nun verschwindet das Aufnahmeformular. Als nächstes wird die Bearbeitung deaktivieret mit einem Klick auf das Stiftsymbol („Bearbeitung umschalten“). Es erscheint ein Fenster in welchem gefragt wird ob die Änderungen gespeichert werden sollen. Mit dem Klick auf Speichern sind die Aufnahmen gespeichert.
Als nächsten kann ein neuer Stichprobenpunkt ausgewählt werden (Rechteck) und die Bearbeitung wieder aktiviert werden (Stift). 
### Geopackage Sperren

Bild 44, 45 und 46

Ein GeoPackage ist eine offene, standardisierte, plattformunabhängige und portable Datenbank, die auf dem SQLite-Datenbankformat basiert. Es ermöglicht die Speicherung und den Austausch geografischer Informationen, einschließlich Vektor- und Rasterdaten.
SQLite, und damit auch GeoPackage, verwendet ein Locking-Mechanismus, um gleichzeitigen Zugriff auf die Datenbank zu ermöglichen und gleichzeitig die Datenintegrität zu gewährleisten. Dieses Locking-System ermöglicht es mehreren Benutzern, gleichzeitig zu lesen, aber nur einem Benutzer, gleichzeitig zu schreiben.
Im Kontext von QGIS und GeoPackage bedeutet dies, dass (theoretisch) mehrere Benutzer gleichzeitig eine GeoPackage-Datei lesen können, aber nur ein Benutzer kann gleichzeitig Änderungen vornehmen. Wenn ein Benutzer beginnt, Änderungen vorzunehmen (zum Beispiel durch das Hinzufügen, Löschen oder Ändern von Features), erhält er ein Schreib-Lock auf die Datenbank. Dies geschieht auch, wenn in einzelnen verbundenen Layern gespeichert wird. Dieses Schreib-Lock verhindert, dass andere Benutzer gleichzeitig Änderungen vornehmen. Sie können weiterhin lesen, aber sie können keine Änderungen vornehmen, bis das Schreib-Lock freigegeben wird. Die Datenbank kann aber nicht erkennen, ob gerade andere gleichzeitig darin arbeiten. Wenn für ein Layer der Befehl für einen insert, in QGIS Speicherung, aufgerufen wird, wird der Zugriff auf die Datenbank für die anderen Transaktionen gesperrt.
Die Butten „Layeränderungen speichern“ (Abb. 42) und „aktuelle Änderungen - in mehreren Layern - speichern“ (Abb.43) rufen den insert-Befehl für die Datenbank auf. Damit wird die Sperre auf die Datenbank aktiviert, um die gleichzeitige Speicherung eines anderen Users oder Prozesses zu verhindern.
In QGIS wird ein Schreib-Lock normalerweise erworben, wenn Sie beginnen, Änderungen an einem Layer vorzunehmen, und freigegeben, wenn Sie die Änderungen speichern oder die Bearbeitung beenden. Wenn Sie versuchen, Änderungen an einem Layer zu speichern, während ein anderer Benutzer ein Schreib-Lock auf die Datenbank hat, erhalten Sie eine Fehlermeldung und Ihre Änderungen werden nicht gespeichert. 
Es ist wichtig zu beachten, dass das Locking-System von SQLite und GeoPackage darauf ausgelegt ist, die Datenintegrität zu schützen und Konflikte zu vermeiden. Die Daten werden gespeichert wenn man nach der Dateneingabe auf „Bearbeitungsstatus umschalten“ (Abb. 44) und dann auf speichern klickt. Damit sind die Daten gesichert und Fehleingaben werden vermieden. Am nächsten Punkt aktiviert man die Bearbeitung wieder durch einen erneuten Klick auf „Bearbeitungsstatus umschalten“


